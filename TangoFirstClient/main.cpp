#include <tango.h>
#include <iostream>

int main()
{
	cout << "Connecting to tango..." << std::endl;

	try
	{
		Tango::DeviceProxy device("sys/tangotest/3");
		double duration = device.ping();
		cout << duration << endl;
		std::string status = device.status();
		cout << status << endl;

		/*
		while (true)
		{
			Tango::DeviceAttribute reply = device.read_attribute("double_scalar");
			double value;
			reply >> value;
			cout << "double_scalar:" << value << endl;
			Sleep(1000);
		}
		*/

		/*
		bool newBoolValue = false;
		Tango::DeviceAttribute boolArgin;
		boolArgin << newBoolValue;
		boolArgin.set_name("boolean_scalar");
		device.write_attribute(boolArgin);
		*/

		Tango::DeviceData argin, argout;
		double newCmdArg = 77;
		argin << newCmdArg;

		argout = device.command_inout("DevDouble", argin);
		double receivedCmdData;
		argout >> receivedCmdData;
		cout << receivedCmdData << endl;
	}
	catch (Tango::DevFailed &e)
	{
		std::cerr << e.errors[0].desc;
	}


	return 0;
}